from __future__ import annotations

from common.interfaces.i_ihm_game_calls_data import I_IHMGameCallsData

from typing import Dict, List, Optional, Tuple, Union
from common import (
    Move,
    LocalGame,
    Player,
    Message,
    Profile,
    GameStatus,
    User,
    data_structures,
)
from uuid import *

import pickle

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from client.data.data_controller import DataController


class IhmGameCallsDataImpl(I_IHMGameCallsData):
    def __init__(self, data_controller: DataController) -> None:
        self.data_controller = data_controller

    def check_move_in_local_game(self, move: Move) -> bool:
        pass

    def leave_local_game(self):
        self.data_controller.local_game = None

    def create_local_game(self):
        mayo = Profile("Mayo", "Server", 80, 111, 77, 90, uuid4())
        self.data_controller.local_game = LocalGame(
            "SuperGame", mayo, 140, 5, GameStatus.AVAILABLE, None, None, 0
        )

    def get_local_game(self) -> LocalGame:
        # test if the local game is already created
        if self.data_controller.local_game is not None:
            return self.data_controller.local_game
        else:
            raise ValueError("Local game not created : Is None.")

    def get_local_game_grid(self) -> Dict:
        return self.get_local_game().board

    def get_local_players(self) -> Tuple[Profile, Profile]:
        # test if players are already created
        red_player: Optional[Profile] = self.get_local_game().red_player
        white_player: Optional[Profile] = self.get_local_game().white_player
        if type(red_player) == Profile and type(white_player) == Profile:
            return white_player, red_player
        else:
            raise ValueError("Some Players are None.")

    def get_local_game_spectators(self) -> List[Player]:
        mayo = Player("Mayo", uuid4())
        return [mayo]

    def get_local_game_messages(self) -> List[Message]:
        # test if the local game is already created
        if self.data_controller.local_game is not None:
            return self.data_controller.local_game.get_chat()
        else:
            raise ValueError("Local game is None.")

    def is_local_game_finished(self) -> bool:
        return self.get_local_game().status == GameStatus.FINISHED

    def get_local_game_winner(self) -> List[Profile]:
        winners: Tuple[
            Optional[Profile], Optional[Profile], List[int]
        ] = self.get_local_game().get_winner()
        res: List[Profile] = []
        for winner in winners:
            if type(winner) == Profile:
                res.append(winner)
        return res

    def clear_local_game(self) -> None:
        self.data_controller.local_game = None

    def get_public_games(self) -> List:
        return self.data_controller.public_games

    def get_connected_players(self) -> List[Player]:
        return self.data_controller.connected_players

    def get_local_game_id(self) -> UUID:
        pass

    def get_profile(self) -> Profile:
        pass

    def create_new_profile(self, user: User) -> None:
        """
        Doesnt 'create' anything.
        Saves the user object to a file.
        Does not check if the nickname is already taken.
        """
        profile_path = DataController.USER_PATH + user.nickname + ".profile"
        with open(profile_path, "wb") as profile_file:
            pickle.dump(user, profile_file)

    def export_current_profile(self, destination_path: str) -> None:
        pass

    def import_profile(self, path: str) -> Profile:
        pass

    def edit_current_profile(self, profile: Profile) -> None:
        pass

    def disconnect_from_app(self) -> None:
        pass

    def disconnect_server(self) -> None:
        pass
